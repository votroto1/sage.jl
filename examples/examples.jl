using JuMP.Containers: @container

include("../src/sage.jl")

"""Compute the shapley values and the value of each coalition"""
function compute_shap_and_vals(p)
	p_fun(x...) = probability(p, x...)
	support = axes(p)

	shap = shapley(p_fun, support)
	vals = coalition_value(p_fun, support)

	shap, vals
end

function print_shap_and_vals(p)
	shap, vals = compute_shap_and_vals(p)

	@show shap
	println("vals: {")
	for (k,v) in vals
		println("\t$k \t=> $(v)")
	end
	println("}")
	println()
end


pR = @container([x1 = [0, 1], x2 = [0, 1], y = [0, 1]], 0.0)
pX = @container([x1 = [0, 1], x2 = [0, 1], y = [0, 1]], 0.0)
p2 = @container([x1 = [0, 1], x2 = [0, 1], y = [0, 1, 2, 3]], 0.0)
pA = @container([x1 = [0, 1], x2 = [0, 1], y = [0, 1]], 0.0)
pS = @container([x1 = [0, 1, 2, 3], x2 = [0, 1, 2, 3], y = [0, 1, 2, 3]], 0.0)

# RDN
pR[0, 0, 0] = 1 / 2
pR[1, 1, 1] = 1 / 2

# XOR
pX[0, 0, 0] = 1 / 4
pX[0, 1, 1] = 1 / 4
pX[1, 0, 1] = 1 / 4
pX[1, 1, 0] = 1 / 4

# 2 Bit Copy
p2[0, 0, 0] = 1 / 4
p2[0, 1, 1] = 1 / 4
p2[1, 0, 2] = 1 / 4
p2[1, 1, 3] = 1 / 4

# ANDqY[1]
pA[0, 0, 0] = 1 / 4
pA[0, 1, 0] = 1 / 4
pA[1, 0, 0] = 1 / 4
pA[1, 1, 1] = 1 / 4

# SynRdn
pS[0, 0, 0] = 1 / 8
pS[0, 1, 1] = 1 / 8
pS[1, 0, 1] = 1 / 8
pS[1, 1, 0] = 1 / 8
pS[2, 2, 2] = 1 / 8
pS[2, 3, 3] = 1 / 8
pS[3, 2, 3] = 1 / 8
pS[3, 3, 2] = 1 / 8

########### 3 input variables

pP = @container([x1 = [0, 1], x2 = [0, 1], x3 = [0, 1], y = [0, 1]], 0.0)
pM = @container([x1 = [0, 1, 2, 3], x2 = [0, 1, 2, 3], x3 = [0, 1, 2, 3], y = [0, 1]], 0.0)
pJ = @container([x1 = [0, 1], x2 = [0, 1], x3 = [0, 1], y = [0, 1, 2, 3]], 0.0)
p3 = @container([x1 = [0, 1], x2 = [0, 1], x3 = [0, 1], y = [0, 1]], 0.0)
pE = @container([x1 = [0, 1], x2 = [0, 1], x3 = [0, 1], y = [0, 1]], 0.0)

# Parity
pP[0, 0, 0, 0] = 1 / 8
pP[0, 0, 1, 1] = 1 / 8
pP[0, 1, 0, 1] = 1 / 8
pP[0, 1, 1, 0] = 1 / 8
pP[1, 0, 0, 1] = 1 / 8
pP[1, 0, 1, 0] = 1 / 8
pP[1, 1, 0, 0] = 1 / 8
pP[1, 1, 1, 1] = 1 / 8

# XORMultiCoal
pM[2, 0, 2, 0] = 1 / 8
pM[0, 2, 1, 0] = 1 / 8
pM[1, 1, 0, 0] = 1 / 8
pM[3, 3, 3, 0] = 1 / 8
pM[3, 1, 2, 1] = 1 / 8
pM[1, 3, 1, 1] = 1 / 8
pM[0, 0, 0, 1] = 1 / 8
pM[2, 2, 3, 1] = 1 / 8

#Rboj
pJ[0, 0, 0, 0] = 1 / 4
pJ[0, 1, 1, 1] = 1 / 4
pJ[1, 0, 1, 2] = 1 / 4
pJ[1, 1, 0, 3] = 1 / 4

# Three Way AND
p3[0, 0, 0, 0] = 1 / 8
p3[0, 0, 1, 0] = 1 / 8
p3[0, 1, 0, 0] = 1 / 8
p3[0, 1, 1, 0] = 1 / 8
p3[1, 0, 0, 0] = 1 / 8
p3[1, 0, 1, 0] = 1 / 8
p3[1, 1, 0, 0] = 1 / 8
p3[1, 1, 1, 1] = 1 / 8

# Example with negative priority values!
pE[0, 0, 0, 0] = 1 / 16
pE[1, 0, 0, 0] = 1 / 16
pE[0, 1, 0, 0] = 1 / 16
pE[0, 0, 1, 0] = 1 / 16
pE[0, 0, 0, 1] = 1 / 16
pE[1, 1, 0, 0] = 0
pE[1, 0, 1, 0] = 0
pE[1, 0, 0, 1] = 0
pE[0, 1, 1, 0] = 0
pE[0, 0, 1, 1] = 0
pE[0, 1, 0, 1] = 6 / 16
pE[1, 1, 1, 0] = 4 / 16
pE[1, 1, 0, 1] = 0
pE[1, 0, 1, 1] = 0
pE[0, 1, 1, 1] = 0
pE[1, 1, 1, 1] = 1 / 16


print_shap_and_vals(pR)
print_shap_and_vals(pX)
print_shap_and_vals(p2)
print_shap_and_vals(pA)
print_shap_and_vals(pS)
print_shap_and_vals(pP)
print_shap_and_vals(pM)
print_shap_and_vals(pJ)
print_shap_and_vals(p3)
print_shap_and_vals(pE)