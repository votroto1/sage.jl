using Base.Iterators: product
using Combinatorics: powerset

zeroifnan(x) = isnan(x) ? 0 : x

"""Generate all of the realizations Xi in support_Xi"""
realizations(i::Integer, sup) = i .=> sup[i]
realizations(i, sup) = map(x -> i => x, product(sup[i]...))


"""extract probability out of p"""
function probability(p, xs...)
	"""Select only cells which agree with observations in xs"""
	function make_constraint(p, xs...)
		viewspec = Array{Any}(undef, ndims(p))
		fill!(viewspec, :)
		for (i, x) in xs
			viewspec[collect(i)] .= collect(x)
		end
		viewspec
	end

        constr = make_constraint(p, xs...)
        sum(p[constr...])
end

"""compute shapley values for each 'player' X"""
function shapley(P, support; N=eachindex(support)[begin:end-1], n=length(N))
        """conditional mutual probability for a coalition S and player i"""
        function I(i, S)
                term((y, xi, xs)) = P(y, xi, xs) * log2(P(xs) * P(y, xi, xs) / (P(y, xs) * P(xi, xs)))

                y = realizations(n + 1, support)
                xi =  realizations(i, support)
                xs =  realizations(S, support)

                sum(zeroifnan ∘ term, product(y, xi, xs); init=0)
        end

	"""Compute the shapley value of a single player i"""
        function phi(i)
                D(i) = powerset(setdiff(N, i))
                B(S) = binomial(n - 1, length(S))

                1 / n * sum(I(i, S) / B(S) for S in D(i); init=0.0)
        end

        [phi(i) for i in N]
end

"""compute values for each 'coalition' from the powerset of xs"""
function coalition_value(P, support; N=eachindex(support)[begin:end-1], n=length(N))
        """conditional mutual probability for a coalition S"""
        function I(S)
                term((y, xs)) = P(y, xs) * log2(P(y, xs) / (P(y) * P(xs)))

                y = realizations(n + 1, support)
                xs =  realizations(S, support)

                sum(zeroifnan ∘ term, product(y, xs); init=0)
        end

        Dict([(tuple(a...), I(a)) for a in powerset(N)])
end

